import './App.css'
import { useState } from 'react'
import TodoList from './components/TodoList'
import TodoListFooter from './components/TodoListFooter'
import NewItemInput from './components/NewItemInput'

const App = () => {
  const [items, setItems] = useState([
    { id: 1, text: 'Buy bread', completed: false },
    { id: 2, text: 'Learn javascript', completed: false },
    { id: 3, text: 'Learn React', completed: true }
  ])
  const [showActive, setShowActive] = useState(false)

  const filteredItems = showActive
   ? items.filter(item => !item.completed)
   : items

  const addTodo = (newTodoText) => {
    if (newTodoText.length === 0) {
      return
    }
    const newTodo = {
      id: Date.now(),
      text: newTodoText,
      completed: false
    }
    setItems([...items, newTodo])
  }
  
  const handleCheck = (e, id) => {
    const completed = e.target.checked
    const newItems = items.map(item => {
      if (item.id === id) {
        item.completed = completed
      }
      return item
    })
    setItems(newItems)
  }

  const handleDelete = (id) => {
    const newItems = items.filter(item => item.id !== id)
    setItems(newItems)
  }

  const handleFilter = (active) => setShowActive(active)

  return (
    <div className="App">
      <h1>Todo List - Hooks</h1>
      <NewItemInput 
        addTodo={addTodo}
      />
      <TodoList
        items={filteredItems}
        handleCheck={handleCheck}
        handleDelete={handleDelete}
      />
      <TodoListFooter 
        nrItemsLeft={items.filter(item => !item.completed).length}
        handleFilter={handleFilter}
      />
    </div>
  );
}

export default App;
