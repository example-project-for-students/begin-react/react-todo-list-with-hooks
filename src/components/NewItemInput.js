import { useState, useRef, useEffect } from 'react'

const NewItemInput = ( { addTodo }) => {
  const [newTodoText, setNewTodoText] = useState('')
  const inputEl = useRef(null)

  const handleChange = e => setNewTodoText(e.target.value)

  const handleSubmit = (e, text) => {
    e.preventDefault()
    setNewTodoText('')
    inputEl.current.focus()
    addTodo(text)
  }

  useEffect(() => {
    inputEl.current.focus()
  }, [])

  return (
    <form onSubmit={(e) => handleSubmit(e, newTodoText)}>
      <label htmlFor="new-todo">Add new task: </label>
      <input
        id="new-todo"
        value={newTodoText}
        onChange={handleChange}
        ref={inputEl}
      />
      <button type="submit">Add</button>
    </form>
  )
}

export default NewItemInput