import './TodoList.css'

const TodoList = ({ items, handleCheck, handleDelete }) => {
  return (
    <div>
      { items.length === 0
      ? <p>Nothing to do today!</p>
      : <ul className="TodoList">
          {
            items.map(item => (
              <li key={item.id}>
                <label>
                  <input
                    type="checkbox"
                    checked={item.completed}
                    onChange={(e) => handleCheck(e, item.id)}
                  />
                  {item.text + '  '}
                </label>
                <button onClick={() => handleDelete(item.id)}>x</button>
              </li>
            ))
          }
        </ul>
      }
    </div>
  )
}

export default TodoList