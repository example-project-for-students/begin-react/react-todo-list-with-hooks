const TodoListFooter = ( { nrItemsLeft, handleFilter }) => {
  return (
    <div>
      <span>{`${nrItemsLeft} items left  `}</span>
      <button onClick={() => handleFilter(false)}>Show all</button>
      <button onClick={() => handleFilter(true)}>Show active</button>
    </div>
  )
}

export default TodoListFooter